/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hadoop1;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author Matt
 */
public class CalculateEntrophyMapper extends Mapper<LongWritable , Text, DoubleWritable, Text> {

    @Override
    protected void map(LongWritable  key, Text passwordValue, Context context) throws IOException, InterruptedException {
        double entrophyValue = getShannonEntropy(passwordValue.toString());
        context.write(new DoubleWritable(entrophyValue), passwordValue);
    }
    
    public static double getShannonEntropy(String s) {
        int n = 0;
        Map<Character, Integer> occ = new HashMap<>();

        for (int c_ = 0; c_ < s.length(); ++c_) {
            char cx = s.charAt(c_);
            if (occ.containsKey(cx)) {
                occ.put(cx, occ.get(cx) + 1);
            } else {
                occ.put(cx, 1);
            }
            ++n;
        }

        double e = 0.0;
        for (Map.Entry<Character, Integer> entry : occ.entrySet()) {
            char cx = entry.getKey();
            double p = (double) entry.getValue() / n;
            e += p * (Math.log(p) / Math.log(2));
        }
        return -e;
    }

}