/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hadoop1;
import java.nio.ByteBuffer;
import org.apache.hadoop.io.WritableComparator;
/**
 *
 * @author ubuntu
 */
public class SortEntrophy extends WritableComparator{

    @Override
    public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
        double val = ByteBuffer.wrap(b1, s1, l1).getDouble();
        double secondVal = ByteBuffer.wrap(b2, s2, l2).getDouble();
        return (-1 * Double.compare(val, secondVal));
    }
    
}

